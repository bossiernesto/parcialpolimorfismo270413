# Parical de Polimorfismo 


## Enunciado

Tenemos un dispenser generico en el que vamos a tener los diferentes productos que vamos a poder adquirir, para eso, cada producto es en realidad una serie de procesos, en los que se va a ir pasando hasta que se sirve el producto. Cada vez que empieza un proceso por el que va a pasar un producto, se va a mostrar el nombre del procedimiento.

## Punto 1

Con un proceso como el que se muestra en el siguiente bloque de codigo, determinar que tipo de comportamiento tienen en comun ambos objetos? y si imprimen lo mismo ambos mensajes?

```
ProcesoA proceso1=new ProcesoA();
		
proceso1.procesar3(new ProcesoB());
proceso1.procesar3(new ProcesoC());

```


## Punto 2

Teniendo en cuenta los siguientes procesos, indicar para cada proceso de la lista cuales son los mensajes que vamos a ver en la pantalla del dispenser.

```

lista= new ArrayList<Proceso>();

proceso1=new ProcesoA(new ProcesoD());
proceso2=new ProcesoA(new ProcesoC()); 
proceso3=new ProcesoB(new ProcesoD());
proceso4=new ProcesoD(new ProcesoB());
lista.add(proceso1);
lista.add(proceso2);
lista.add(proceso3);
lista.add(proceso4);

dispenser= new Dispenser(lista);
dispenser.procesarTodo();


```

