package parcial;

public abstract class Proceso {

	protected Proceso proceso;

	public Proceso(Proceso unProceso) {
		this.proceso = unProceso;
	}

	public Proceso() {
	}

	public abstract void procesar();

	public abstract void procesar4();

	public abstract void procesar5();

	public abstract void procesar5(Proceso unProceso);

}
