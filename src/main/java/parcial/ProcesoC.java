package parcial;


public class ProcesoC extends Proceso {

	public ProcesoC(Proceso unProceso) {
		super(unProceso);
	}

	public ProcesoC() {
		super();
	}

	public void procesar() {
		this.procesar3();
		this.procesar4(this);
	}

	public void procesar3() {
		System.out.println("Procesando Paso C3...");
	}

	public void procesar4(Proceso unProceso) {
		System.out.println("Procesando Paso C4 -> P...");
		unProceso.procesar4();
	}

	public void procesar4(ProcesoC unProcesoc){
		System.out.println("Procesando Paso C4 -> PC...");
		unProcesoc.procesar5();
	}
	
	public void procesar4(ProcesoD unProcesod){
		System.out.println("Procesando Paso C4 -> PD...");
		unProcesod.procesar5();
	}
	
	public void procesar4() {
		System.out.println("Procesando Paso C4...");
	}

	public void procesar5() {
		System.out.println("Procesando Paso C5...");
	}

	public void procesar5(Proceso unProceso) {
		unProceso.procesar4();
	}

	public void procesar5(ProcesoA unProcesoA) {
		unProcesoA.procesar3();
	}
}
