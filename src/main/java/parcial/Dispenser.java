package parcial;

import java.util.List;

public class Dispenser {

	private List<Proceso> procesos;

	public Dispenser(List<Proceso> unaListaProcesos) {
		this.procesos = unaListaProcesos;
	}

	public void procesarTodo() {
		for (Proceso proceso : this.procesos) {
			proceso.procesar();
			this.servir();
		}

	}

	public void servir() {
		System.out.println("Sirviendo....");
	}

}
