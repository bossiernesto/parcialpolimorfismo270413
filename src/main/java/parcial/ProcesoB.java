package parcial;

public class ProcesoB extends ProcesoA {

	public ProcesoB(Proceso unProceso) {
		super(unProceso);
	}

	public ProcesoB() {
		super();
	}

	public void procesar() {
		this.procesar3(this.proceso);
		this.procesar3();
		this.proceso.procesar4();
	}

	public void procesar4() {
		System.out.println("Procesando Paso B4 ...");
	}

	public void procesar3() {
		System.out.println("Procesando Paso B3...");
	}

}
