package parcial;


public class ProcesoA extends Proceso {

	public ProcesoA(Proceso unProceso) {
		super(unProceso);
	}

	public ProcesoA() {
		super();
	}

	public void procesar() {
		this.procesar2();
		this.proceso.procesar4();
		this.proceso.procesar5();
	}

	private void procesar2() {
		System.out.println("Procesando Paso A2...");
	}

	public void procesar4() {
		this.proceso.procesar4();
		this.proceso.procesar5();
	}

	public void procesar3(Proceso unProceso) {
		System.out.println("Procesando Paso A3...");
	}

	public void procesar3(ProcesoC unProcesoc) {
		this.procesar5(unProcesoc);
		unProcesoc.procesar4();
	}

	public void procesar3() {
		this.procesar5();
	}

	public void procesar5() {
		this.procesar2();

	}

	public void procesar5(Proceso unProceso) {
		unProceso.procesar();
	}

	public void procesar5(ProcesoD unProcesod) {
		unProcesod.procesar4();
	}

}
