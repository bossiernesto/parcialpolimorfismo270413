package test;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;

import parcial.Dispenser;
import parcial.Proceso;
import parcial.ProcesoA;
import parcial.ProcesoB;
import parcial.ProcesoC;
import parcial.ProcesoD;

public class DispenserTest extends TestCase {

	protected Dispenser dispenser;
	protected ProcesoA procesoA;
	protected ProcesoA procesoA2;
	protected ProcesoB procesoC;
	protected ProcesoD procesoD;
	protected List<Proceso> list;

	protected void setUp() {
		this.dispenser = new Dispenser(this.CrearProcesos());
	}
	
	
	protected List<Proceso> CrearProcesos(){
		this.list= new ArrayList<Proceso>();
		//Creo los procesos
		this.procesoA=new ProcesoA(new ProcesoD());
		this.procesoA2=new ProcesoA(new ProcesoC()); 
		this.procesoC=new ProcesoB(new ProcesoD());
		this.procesoD=new ProcesoD(new ProcesoB());

		this.list.add(this.procesoA);
		this.list.add(this.procesoA2);
		this.list.add(this.procesoC);
		this.list.add(this.procesoD);
		return this.list;
	}

	// Tests
	/*
	 * Punto 1 del enunciado:
	 *
	 *
	 * Punto 1 del enunciado:Explicar el comportamiento que tiene el proceso 1 cuando le mandamos
	 * estos mensajes.Imprimen lo mismo los dos mensajes?
	 *
	 * El primero imprime Procesando Paso 3... Procesando Paso 4C con Proceso... Procesando Paso 5...
	 * Procesando Paso 4...
	 *
	 * y el segundo... Procesando Paso 3...
	 */
	@Test
	public void testProceso() {
		ProcesoA proceso1 = new ProcesoA();

		System.out.println('-');
		proceso1.procesar3(new ProcesoC());
		System.out.println('-');
		proceso1.procesar3(new ProcesoB());
	}

	/*
	 * Punto 2 del enunciado:
	 *
	 * this.procesoA=new ProcesoA(new ProcesoD()); para este paso Imprime Procesando Paso 2...
	 * Procesando Paso 4... Procesando Paso 5-2 del D... Sirviendo....
	 *
	 *
	 * this.procesoA2=new ProcesoA(new ProcesoC()); Procesando Paso 2... Procesando Paso 4...
	 * Procesando Paso 5...
	 *
	 * this.procesoC=new ProcesoB(new ProcesoD()); para este paso Imprime Procesando Paso 3...
	 * Procesando Paso 3-2... Procesando Paso 4...
	 *
	 * this.procesoD=new ProcesoD(new ProcesoC()); para este paso Imprime Sirviendo.... Procesando
	 * Paso 5-2 del D... Procesando Paso 3... Sirviendo....
	 */
	@Test
	public void testTodo() {
		System.out.println("2-");
		this.dispenser.procesarTodo();
	}

}
